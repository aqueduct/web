const form = document.getElementById("register-form");

const emailInput = /** @type {HTMLInputElement | null} */ (
  document.getElementById("register-form-email-input")
);

const usernameInput = /** @type {HTMLInputElement | null} */ (
  document.getElementById("register-form-username-input")
);

const passwordInput = /** @type {HTMLInputElement | null} */ (
  document.getElementById("register-form-password-input")
);

const errorElement = document.getElementById("register-form-error");

if (
  form == null ||
  emailInput == null ||
  usernameInput == null ||
  passwordInput == null ||
  errorElement == null
) {
  throw new Error("One of the HTML elements are missing.");
}

form.addEventListener("submit", (event) => {
  event.preventDefault();
  const email = emailInput.value.trim();
  const username = usernameInput.value.trim();
  const password = passwordInput.value.trim();

  if (email == "" || username == "" || password == "") {
    errorElement.innerText = "Must specify all fields.";
    return;
  }

  apiRegister({
    email,
    password,
    username,
  })
    .then((user) => {
      const div = document.createElement("div");
      div.classList.add("form");

      const p = document.createElement("p");
      p.innerText = `Congratulations on registering! You are now ${user.username}#${user.discriminator}. The numbers after the pound sign are your discriminator. They are used to differentiate between you and other users with the same username.`;

      const button = document.createElement("button");
      button.innerText = "Go to login page";
      button.onclick = () => location.replace("/login");

      div.appendChild(p);
      div.appendChild(button);

      form.replaceWith(div);
    })
    .catch((response) => {
      response.json().then(
        /**
         * @param {{ error: string }} obj
         */
        (obj) => {
          errorElement.innerText = obj.error;
        },
      );
    });
});
