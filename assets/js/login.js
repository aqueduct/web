const form = document.getElementById("login-form");

const emailInput = /** @type {HTMLInputElement | null} */ (
  document.getElementById("login-form-email-input")
);

const passwordInput = /** @type {HTMLInputElement | null} */ (
  document.getElementById("login-form-password-input")
);

const errorElement = document.getElementById("login-form-error");

if (
  form == null ||
  emailInput == null ||
  passwordInput == null ||
  errorElement == null
) {
  throw new Error("One of the HTML elements are missing.");
}

const friendly_name = "Aqueduct Web";

form.addEventListener("submit", (event) => {
  event.preventDefault();
  const email = emailInput.value.trim();
  const password = passwordInput.value.trim();

  if (email == "" || password == "") {
    errorElement.innerText = "Must specify all fields.";
    return;
  }

  apiSessionLogin({
    email,
    password,
    friendly_name,
  })
    .then((session) => {
      localStorage.setItem("session", JSON.stringify(session));
      location.replace("/");
    })
    .catch((response) => {
      response.json().then(
        /**
         * @param {{ error: string }} obj
         */
        (obj) => {
          errorElement.innerText = obj.error;
        },
      );
    });
});
