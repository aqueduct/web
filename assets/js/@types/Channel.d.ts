type Channel = {
  _id: string;
  type: "DM";
  recipient_ids: [string, string];
};