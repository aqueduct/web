interface DataSendMessage {
  /**
   * Message content.
   *
   * Maximum length is 2000 characters.
   */
  content?: string;

  /**
   * Message attachments.
   *
   * Maximum length is 5.
   */
  attachments?: string[];

  /**
   * Message replies
   *
   * Maximum length is 5.
   */
  replies?: string[];
}

interface DataQueryMessages {
  limit?: number;
}

interface Message {
  /**
   * Message ID.
   */
  _id: string;

  /**
   * Channel ID in which this message was sent.
   */
  channel_id: string;

  /**
   * Message content.
   *
   * Maximum length is 2000 characters.
   */
  content?: string;
}