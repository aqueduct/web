interface User {
  _id: string;
  username: string;
  discriminator: string;
}
