interface DataLogin {
  email: string;
  password: string;
  friendly_name?: string;
}

interface DataRegister {
  email: string;
  password: string;
  username: string;
}
