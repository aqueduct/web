interface Session {
  /// Session ID.
  _id: string;

  /// Account ID associated to the session.
  account_id: string;

  /// Session token.
  token: string;

  /// Session name to identify session origin.
  name?: string;
}
