type PingPongPacket = Uint8Array | number;

type WebSocketEvent =
  | {
      /**
       * Response to the `Ping` event in `ClientMessage`.
       *
       * Used to keep the connection alive.
       */
      type: "Pong";
      data: PingPongPacket;
    }
  | ({
      /**
       * Event sent when a message is sent to a channel
       */
      type: "Message";
    } & Message)
  | {
      type: "Ready";
      channels: Channel[];
    };

/**
 * Event from client to server.
 */
type ClientMessage = {
  /**
   * Event to keep the connection alive.
   */
  type: "Ping";
  data: PingPongPacket;
};