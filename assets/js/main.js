const serverTitleElement = document.getElementById("serverTitle");
const channelListContainer = document.getElementById("channelList");
const userSettingsElement = document.getElementById("userSettings");
const messageList = document.getElementById("messageList");
const messageBox = document.getElementById("messageBox");

if (
  serverTitleElement == undefined ||
  channelListContainer == undefined ||
  userSettingsElement == undefined ||
  messageList == undefined ||
  messageBox == undefined
) {
  throw new Error("one of the html elements was not found");
}

// -- mock data --
// replace later

const selectedServer = {
  name: "Aqueduct Internal Instance",
};

/**
  @type {Channel[]}
 */
let channels = [];

function updateChannelUI() {
  channelListContainer?.replaceChildren(
    ...channels.map((channel, index) => {
      const element = document.createElement("div");
      element.classList.add("channel");
      element.id = "C" + channel._id;
      element.role = "link";

      const hashIcon = document.createElement("i");
      hashIcon.classList.add("ph-bold", "ph-hash");

      element.append(hashIcon, "Channel " + (index + 1));

      if (selectedChannelID == undefined) {
        selectChannel(channel._id, element);
      }

      element.addEventListener("click", () => {
        if (selectedChannelID == channel._id) {
          return;
        }

        selectChannel(channel._id, element);
      });

      return element;
    }),
  );
}

/**
 * @param {Channel[]} value
 */
function setChannels(value) {
  channels = value;
  updateChannelUI();
}

client.on("Ready", (event) => {
  setChannels(event.channels);
});

// -- end mock data --

serverTitleElement.innerText = selectedServer.name;

/**
 * Currently selected channel ID.
 * @type {string | undefined}
 */
let selectedChannelID = channels[0]?._id;

// initialize the initially-selected channel
if (selectedChannelID != undefined) {
  selectChannel(selectedChannelID);
}

userPromise.then((user) => {
  const userAvatarElement = document.createElement("img");
  // userAvatarElement.src = user?.avatarURL;
  userAvatarElement.classList.add("user-pfp");

  const userInfoContainer = document.createElement("div");
  userInfoContainer.classList.add("user-text-container");

  const userHandleElement = document.createElement("div");
  userHandleElement.classList.add("user-handle");
  userHandleElement.innerText = "@" + user.username + "#" + user.discriminator;

  userInfoContainer.append(userHandleElement);

  // if (user.status?.text != undefined) {
  // const userStatusElement = document.createElement("div");
  // userStatusElement.classList.add("user-status");
  // userStatusElement.innerText = user.status.text;
  // userInfoContainer.append(userStatusElement);
  // }

  userSettingsElement.append(userAvatarElement, userInfoContainer);
});

messageBox.addEventListener("keydown", (event) => {
  if (
    event.key == "Enter" &&
    !event.shiftKey &&
    selectedChannelID != undefined
  ) {
    event.preventDefault();

    apiSendMessage(selectedChannelID, {
      content: messageBox.innerText.trim(),
    }).then(() => messageBox.replaceChildren());
  }
});

client.on("Message", (message) => {
  if (message.channel_id == selectedChannelID) {
    pushMessage(message);
  }
});
