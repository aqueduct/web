/**
 * @template {{ type: string }} E
 * @typedef {{ [K in E['type']]: Extract<E, { type: K }> }} SocketEventsToEmitterEvents
 */

/**
 * @type {WebSocket | undefined}
 */
let rawWebSocket = new WebSocket("ws://127.0.0.1:8000/ws");

const WS_PING_HEARTBEAT_INTERVAL = 30000;
const WS_PONG_TIMEOUT = 10000;

/**
 * Sends a WebSocket message to the server.
 * @param {ClientMessage} message Event to send
 */
function sendWebSocketMessage(message) {
  if (rawWebSocket == undefined) {
    console.error(
      "Tried to send a WebSocket message when the connection was disconnected:",
      message,
    );
    return;
  }

  rawWebSocket.send(JSON.stringify(message));
}

function disconnectWebSocket() {
  rawWebSocket?.close();
  rawWebSocket = undefined;
}

/**
 * @type {NodeJS.Timeout | undefined}
 */
let wsPingIntervalReference = undefined;

/**
 * @type {NodeJS.Timeout | undefined}
 */
let wsPongTimeoutReference = undefined;

/**
 * @type {EventEmitter<SocketEventsToEmitterEvents<WebSocketEvent>>}
 */
const client = new EventEmitter();

rawWebSocket.addEventListener("open", () => {
  wsPingIntervalReference = setInterval(() => {
    sendWebSocketMessage({ type: "Ping", data: Date.now() });
    wsPongTimeoutReference = setTimeout(disconnectWebSocket, WS_PONG_TIMEOUT);
  }, WS_PING_HEARTBEAT_INTERVAL);
});

rawWebSocket.addEventListener("close", disconnectWebSocket);

/**
 *
 * @param {WebSocketEvent} message
 */
function handleWebSocketMessage(message) {
  switch (message.type) {
    case "Pong": {
      clearTimeout(wsPongTimeoutReference);
      break;
    }
  }

  client.emit(message.type, message);
}

rawWebSocket.addEventListener(
  "message",
  /**
   *
   * @param {MessageEvent<Blob>} message
   */
  (message) => {
    message.data.text().then((data) => {
      handleWebSocketMessage(JSON.parse(data));
    });
  },
);