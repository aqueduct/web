// Small package to manage ULIDs

// These values should NEVER change. If
// they do, we're no longer making ULIDs!
const ULID_ENCODING = "0123456789ABCDEFGHJKMNPQRSTVWXYZ"; // Crockford's Base32

// The maximum timestamp a ULID can encode
const ULID_TIME_MAX = 281474976710655;

// The length of the ULID timestamp component
const ULID_TIME_LEN = 10;

// The length of the ULID random component
const ULID_RANDOM_LEN = 16;

/**
 * Function to generate random cryptographic numbers for ULIDs.
 * @returns {number} Random value
 */
function ulidPrng() {
  const buffer = new Uint8Array(1);
  window.crypto.getRandomValues(buffer);
  return buffer[0] / 0xff;
}

/**
 * Function to generate a random character from the ULID Base32 encoding.
 * @returns {string} 1 character
 */
function ulidRandomChar() {
  let rand = Math.floor(ulidPrng() * ULID_ENCODING.length);
  if (rand == ULID_ENCODING.length) {
    rand = ULID_ENCODING.length - 1;
  }

  return ULID_ENCODING.charAt(rand);
}

/**
 * Encode Unix timestamp into a ULID component.
 * @param {number} timestamp Unix timestamp to encode, defaults to Date.now()
 * @returns {string}
 */
function ulidEncodeTime(timestamp = Date.now()) {
  if (timestamp > ULID_TIME_MAX) {
    throw new Error("Cannot encode time greater than " + timestamp);
  }

  let s = "";
  for (let i = ULID_TIME_LEN; i > 0; i--) {
    const encodingIndex = timestamp % ULID_ENCODING.length;
    s = ULID_ENCODING.charAt(encodingIndex) + s;
    timestamp = (timestamp - encodingIndex) / ULID_ENCODING.length;
  }

  return s;
}

/**
 * Encode the random ULID component into a string
 * @returns {string}
 */
function ulidEncodeRandom() {
  let s = "";
  for (let i = ULID_RANDOM_LEN; i > 0; i--) {
    s = ulidRandomChar() + s;
  }

  return s;
}

/**
 * Decode the time component of a ULID.
 * @param {string} id
 * @returns {number}
 */
function ulidDecodeTime(id) {
  if (
    id.length != ULID_TIME_LEN &&
    id.length != ULID_TIME_LEN + ULID_RANDOM_LEN
  ) {
    throw new Error(
      "ID to decode is neither a time component of a ULID or a ULID"
    );
  }

  const time = id
    .substring(0, ULID_TIME_LEN)
    .split("")
    .reverse()
    .reduce((carry, char, index) => {
      const encodingIndex = ULID_ENCODING.indexOf(char);
      if (encodingIndex == -1) {
        throw new Error("Invalid character found in ULID: " + char);
      }

      return carry + encodingIndex * Math.pow(ULID_ENCODING.length, index);
    }, 0);

  if (time > ULID_TIME_MAX) {
    throw new Error("Malformed ULID, timestamp too large");
  }

  return time;
}

/**
 * Generates full ULID from unix timestamp
 * @param {number | undefined} timestamp Unix timestamp to encode into the ULID, defaults to Date.now()
 * @returns {string}
 */
function ulid(timestamp = Date.now()) {
  return ulidEncodeTime(timestamp) + ulidEncodeRandom();
}
