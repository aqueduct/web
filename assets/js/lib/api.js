// API configuration, likely to be refactored out to something on the backend.
const API_HOST = "127.0.0.1:8000";
const SECURE_CONNECTION = false;

/**
 * Create an API request.
 * @param {'GET' | 'HEAD' | 'POST' | 'PUT' | 'DELETE' | 'CONNECT' | 'OPTIONS' | 'TRACE' | 'PATCH'} method The HTTP method to use
 * @param {string} path The pathname for the request
 * @param {string | undefined} [body] Optional request body
 * @param {HeadersInit | undefined} [headers] Optional request headers
 * @returns {Promise<Response>}
 */
async function apiReq(method, path, body, headers) {
  const response = await fetch(
    "http" + (SECURE_CONNECTION ? "s" : "") + "://" + API_HOST + path,
    {
      method,
      body,
      headers:
        session == undefined
          ? headers
          : {
              Authorization: "Bearer " + session.token,
              ...(headers || {}),
            },
    },
  );

  if (!response.ok) {
    throw response;
  }
  return response;
}

/**
 * Sends a message to a specified `channel_id`.
 * @param {string} target ID of the channel to send this message on
 * @param {DataSendMessage} data Request data for sending a message
 * @returns {Promise<Message>}
 */
async function apiSendMessage(target, data) {
  const response = await apiReq(
    "POST",
    "/channels/" + target + "/messages",
    JSON.stringify(data),
  );
  return await response.json();
}

/**
 * @param {string} target ID of the channel to query messages from
 * @param {DataQueryMessages} data
 * @returns {Promise<Message[]>}
 */
async function apiQueryMessages(target, data = {}) {
  const params = new URLSearchParams(
    Object.entries(data).flatMap((map) => {
      const key = map[0];
      const value = map[1];
      return value == undefined ? [] : [[key, value.toString()]];
    }),
  );

  const response = await apiReq(
    "GET",
    "/channels/" +
      target +
      "/messages" +
      (params.size == 0 ? "" : "?" + params),
  );
  return await response.json();
}

async function apiFetchCurrentUser() {
  const response = await apiReq("GET", "/users/@me");
  return await response.json();
}

/**
 * @param {DataLogin} data
 * @returns {Promise<Session>}
 */
async function apiSessionLogin(data) {
  const response = await apiReq(
    "POST",
    "/auth/session/login",
    JSON.stringify(data),
  );
  return await response.json();
}

/**
 * @param {DataRegister} data
 * @returns {Promise<User>}
 */
async function apiRegister(data) {
  const response = await apiReq(
    "POST",
    "/auth/account/register",
    JSON.stringify(data),
  );
  return await response.json();
}
