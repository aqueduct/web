/**
 * Current user session.
 * @type {Session | undefined}
 */
let session = undefined;

const sessionText = localStorage.getItem("session");
if (sessionText == null) {
  if (
    !location.pathname.startsWith("/login") &&
    !location.pathname.startsWith("/register")
  ) {
    location.replace("/login");
  }
} else {
  session = JSON.parse(sessionText);
}
