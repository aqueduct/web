/**
 * Replaces a character at a specfic index with a string
 * @param {string} s The string to replace
 * @param {number} index The index at which to replace
 * @param {string} chars The character(s) to replace with
 * @returns {string}
 */
function replaceCharsAt(s, index, chars) {
  if (index >= s.length) {
    return s;
  }

  return s.substring(0, index) + chars + s.substring(index + 1);
}

/**
 * Adds a message to the UI
 * @param {Message} message
 */
function pushMessage(message) {
  const element = document.createElement("div");
  element.classList.add("message-container");

  const avatarElement = document.createElement("img");
  avatarElement.classList.add("user-pfp");

  // use the current user's avatar for now
  // avatarElement.src = user.avatarURL;

  element.append(avatarElement, message.content || "");
  messageList?.appendChild(element);
}

/**
 * Refreshes the message list by querying the API for messages.
 */
function refreshMessageList() {
  messageList?.replaceChildren();
  if (selectedChannelID == undefined) {
    return;
  }

  apiQueryMessages(selectedChannelID).then((messages) =>
    messages.forEach((message) => pushMessage(message)),
  );
}

/**
 * Element of the currently selected channel item.
 * @type {HTMLElement | undefined}
 */
let selectedChannelIDElement = undefined;

/**
 * Selects a channel from a specified ID.
 * @param {string} id Channel ID
 * @param {HTMLElement} [element] The element to update the UI for, can be omitted, but is recommended to be passed if in possesion of the callee to reduce time spent finding the element.
 * @returns {void}
 */
function selectChannel(id, element) {
  if (!element) {
    element = document.getElementById("C" + id) || undefined;
  }

  // remove the active class from the original channel if it exists
  selectedChannelIDElement?.classList.remove("channel-active");

  // update the data
  selectedChannelID = id;
  selectedChannelIDElement = element;

  // add the active class to the new element
  selectedChannelIDElement?.classList.add("channel-active");

  refreshMessageList();
}
