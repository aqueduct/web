// Minimal package to create event emitters

/**
 * @template D
 * @typedef {(data: D) => void} EventHandler
 */

/**
 * @template {Record<string, unknown>} EventTypes
 */
class EventEmitter {
  /**
   * @type {Partial<{ [K in keyof EventTypes]: { fn: Function; once: boolean }[] }>}
   * @private
   */
  _events = {};

  /**
   * Listens to a specific event and calls the handler when it's emitted.
   * @template {keyof EventTypes} K
   * @param {K} event The event to listen to
   * @param {EventHandler<EventTypes[K]>} fn The handler function for the event
   * @param {boolean} once Whether to drop the event handler when it's called once
   * @returns A function to unsubscribe from the event
   */
  on(event, fn, once = false) {
    const listeners = this._events[event] || [];
    const index = listeners.length;
    listeners.push({ fn, once });
    this._events[event] = listeners;

    return () => {
      delete this._events[event]?.[index];
    };
  }

  /**
   * Listens to a specific event, calls the handler when it's emitted and is only run once.
   * @template {keyof EventTypes} K
   * @param {K} event The event to listen to
   * @param {EventHandler<EventTypes[K]>} fn The handler function for the event
   * @returns A function to unsubscribe from the event
   */
  once(event, fn) {
    return this.on(event, fn, true);
  }

  /**
   * Emits an event and calls its event handlers
   * @template {keyof EventTypes} K
   * @param {K} event The event to emit
   * @param {EventTypes[K]} data The data to pass into handler functions
   */
  emit(event, data) {
    const listeners = this._events[event] || [];
    for (let i = 0; i < listeners.length; i++) {
      listeners[i].fn(data);
      if (listeners[i].once) {
        delete listeners[i];
      }
    }
  }
}
