/**
 * @type {User | undefined}
 */
let user = undefined;

/**
 * @type {Promise<User>}
 */
let userPromise;

try {
  userPromise = apiFetchCurrentUser().then((u) => (user = u));
} catch (response) {
  console.log("Loaded `user.js` incorrectly:", response);
}
